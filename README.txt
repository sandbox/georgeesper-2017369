Commerce Tax Canada module for Drupal 7


DESCRIPTION
-----------

  Commerce Tax Canada builds on the Drupal Commerce's Commerce Tax module for
  Canadian retails sales taxes. It deals with simple retail sales taxes and can
  be extended to support less common ones Canadian tax jurisdictions.

  Retail sales tax coverage is not complete, but will cover most situations.
  For example, the Ontario tax rule that exempts the provincial tax component
  for sales of food under $4 has not been implemented.

  The following retail sales taxes are supported:
    NL: HST, RST(vehicles)
    NS: HST
    PE: HST
    NB: HST, PVT, TPPT
    QC: GST, QST
    ON: HST, GST
    MB: GST, RST, PVT
    SK: GST, PST, LCT
    AB: GST
    BC: GST, PST
    YT: GST
    NT: GST
    NU: GST

  Legend:
    GST  = Goods and Services Tax
    HST  = Harmonized Sales Tax
    LCT  = Liquor Consumption Tax
    PST  = Provincial Sales Tax
    PVT  = Provincial Vehicle Tax
    QST  = Quebec Sales Tax
    RST  = Retail Sales Tax
    TPPT = Tangible Personal Property Tax


FEATURES
--------

  - Fields are created for each tax and grouped under each jurisdiction
  - A single field collection is added to each Commerce Product bundle
  - Commerce Tax rates are defined for each tax in each jurisdiction
  - Taxes are configurable for each jusdiction on a product level
  - Rules are used to calculate taxes on cart line items


INSTALLATION
------------

  - Download and install the Commerce Canada Module
  - Go to "Administer > Modules" and enable the module
  - A field named 'Canadian Sales Taxes' (field_sales_taxes_ca) is
    automatically added to all Commerce Product types, with default settings
    for applicable taxes. You need to edit and save the tax settings for each
    product and save it. Note that the tax field instances do not exist until
    you save the product.

  Module Dependencies:
    Drupal core 7.x
    Commerce Line Item
    Commerce Order
    Commerce Tax
    Commerce Tax UI
    Entity
    Field
    Field Collection
    List
    Options
    Payment
    Rules


CONFIGURATION
-------------

Tax rates are configured using Commerce Tax Rate UI:
  "Store > Configuration > Taxes"
  .../admin/commerce/config/taxes

Rules are configured using the Rules UI:
  "Configuration > Workflow > Rules > Components"
  .../admin/config/workflow/rules/components


SUPPORT
-------

  Project page: TBD
  Project issue queue: TBD

  Sponsored by:
    Esper Consulting Inc., Ottawa, Canada
    http://www.esper.ca

  Maintainers:
    - George Esper:   https://drupal.org/user/725082
    - Antony Lovric:  https://drupal.org/user/1302300
    - Omar Touma:     https://drupal.org/user/1807142


CREDITS
-------
  Commerce Tax Canada was inspired by the Commerce Canadian Taxes module.