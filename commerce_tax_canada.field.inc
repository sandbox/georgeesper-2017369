<?php
/**
 * @file
 * commerce_tax_canada.field.inc
 */

/**
 * Implements hook_field_info().
 */
function commerce_tax_canada_field_info() {

  return array_merge(
          _default_root_collection_field_info(),
          _default_admin_area_collection_field_info(),
          _default_tax_field_info());

} // commerce_tax_canada_field_info()

/**
 *  Returns the list of fields used by the Commerce Tax Canada module
 *
 */
function _default_root_collection_field_info() {

  $fields = array();

  // Field Collection - this entity will hold all individual tax fields
  $fields['field_sales_taxes_ca'] = array(
    'field_name' => 'field_sales_taxes_ca',
    'label' => t('Canadian Sales Taxes'),
    'description' => t('Check off the applicable Canadian sales taxes in each jurisdiction for this product.'),
    'type' => 'field_collection',
    'module' => 'field_collection',
    'cardinality' => 1,
    'translatable' => FALSE,
    'settings' => array(
        'hide_blank_items' => 1,
        'path' => '',
    ),
    'instance_settings' => array(),
    'default_widget' => array(
        'type' => 'hidden',
        'label' => t('Canadian Sales Taxes'),
        'settings' => array(),
      ),
    'default_formatter' => '',
  );

  return $fields;
} // _default_root_collection_field_info()

function _default_admin_area_collection_field_info() {

  $fields = array();

  // Field Collection for NL sales taxes
  $fields['field_sales_taxes_ca_nl'] = array(
    'field_name' => 'field_sales_taxes_ca_nl',
    'label' => t('NL Sales Taxes'),
    'description' => t('Newfoundland & Labrador sales taxes. Check off all that apply.'),
    'type' => 'field_collection',
    'module' => 'field_collection',
    'cardinality' => 1,
    'translatable' => FALSE,
    'settings' => array(
        'hide_blank_items' => 1,
        'path' => '',
    ),
    'instance_settings' => array(),
    'default_widget' => array(
        'type' => 'hidden',
        'label' => t('NL Sales Taxes'),
        'settings' => array(),
      ),
    'default_formatter' => '',
  );

  // Field Collection for NS sales taxes
  $fields['field_sales_taxes_ca_ns'] = array(
    'field_name' => 'field_sales_taxes_ca_ns',
    'label' => t('NS Sales Taxes'),
    'description' => t('Nova Scotia sales taxes. Check off all that apply.'),
    'type' => 'field_collection',
    'module' => 'field_collection',
    'cardinality' => 1,
    'translatable' => FALSE,
    'settings' => array(
        'hide_blank_items' => 1,
        'path' => '',
    ),
    'instance_settings' => array(),
    'default_widget' => array(
        'type' => 'hidden',
        'label' => t('NS Sales Taxes'),
        'settings' => array(),
      ),
    'default_formatter' => '',
  );

  // Field Collection for PE sales taxes
  $fields['field_sales_taxes_ca_pe'] = array(
    'field_name' => 'field_sales_taxes_ca_pe',
    'label' => t('PE Sales Taxes'),
    'description' => t('Prince Edward Island sales taxes. Check off all that apply.'),
    'type' => 'field_collection',
    'module' => 'field_collection',
    'cardinality' => 1,
    'translatable' => FALSE,
    'settings' => array(
        'hide_blank_items' => 1,
        'path' => '',
    ),
    'instance_settings' => array(),
    'default_widget' => array(
        'type' => 'hidden',
        'label' => t('PE Sales Taxes'),
        'settings' => array(),
      ),
    'default_formatter' => '',
  );

  // Field Collection for NB sales taxes
  $fields['field_sales_taxes_ca_nb'] = array(
    'field_name' => 'field_sales_taxes_ca_nb',
    'label' => t('NB Sales Taxes'),
    'description' => t('New Brunswick sales taxes. Check off all that apply.'),
    'type' => 'field_collection',
    'module' => 'field_collection',
    'cardinality' => 1,
    'translatable' => FALSE,
    'settings' => array(
        'hide_blank_items' => 1,
        'path' => '',
    ),
    'instance_settings' => array(),
    'default_widget' => array(
        'type' => 'hidden',
        'label' => t('NB Sales Taxes'),
        'settings' => array(),
      ),
    'default_formatter' => '',
  );

  // Field Collection for QC sales taxes
  $fields['field_sales_taxes_ca_qc'] = array(
    'field_name' => 'field_sales_taxes_ca_qc',
    'label' => t('QC Sales Taxes'),
    'description' => t('Quebec sales taxes. Check off all that apply.'),
    'type' => 'field_collection',
    'module' => 'field_collection',
    'cardinality' => 1,
    'translatable' => FALSE,
    'settings' => array(
        'hide_blank_items' => 1,
        'path' => '',
    ),
    'instance_settings' => array(),
    'default_widget' => array(
        'type' => 'hidden',
        'label' => t('QC Sales Taxes'),
        'settings' => array(),
      ),
    'default_formatter' => '',
  );

  // Field Collection for ON sales taxes
  $fields['field_sales_taxes_ca_on'] = array(
    'field_name' => 'field_sales_taxes_ca_on',
    'label' => t('ON Sales Taxes'),
    'description' => t('Ontario sales taxes. Check off all that apply.'),
    'type' => 'field_collection',
    'module' => 'field_collection',
    'cardinality' => 1,
    'translatable' => FALSE,
    'settings' => array(
        'hide_blank_items' => 1,
        'path' => '',
    ),
    'instance_settings' => array(),
    'default_widget' => array(
        'type' => 'hidden',
        'label' => t('ON Sales Taxes'),
        'settings' => array(),
      ),
    'default_formatter' => '',
  );

  // Field Collection for MB sales taxes
  $fields['field_sales_taxes_ca_mb'] = array(
    'field_name' => 'field_sales_taxes_ca_mb',
    'label' => t('MB Sales Taxes'),
    'description' => t('Manitoba sales taxes. Check off all that apply.'),
    'type' => 'field_collection',
    'module' => 'field_collection',
    'cardinality' => 1,
    'translatable' => FALSE,
    'settings' => array(
        'hide_blank_items' => 1,
        'path' => '',
    ),
    'instance_settings' => array(),
    'default_widget' => array(
        'type' => 'hidden',
        'label' => t('MB Sales Taxes'),
        'settings' => array(),
      ),
    'default_formatter' => '',
  );

  // Field Collection for SK sales taxes
  $fields['field_sales_taxes_ca_sk'] = array(
    'field_name' => 'field_sales_taxes_ca_sk',
    'label' => t('SK Sales Taxes'),
    'description' => t('Saskatchewan sales taxes. Check off all that apply.'),
    'type' => 'field_collection',
    'module' => 'field_collection',
    'cardinality' => 1,
    'translatable' => FALSE,
    'settings' => array(
        'hide_blank_items' => 1,
        'path' => '',
    ),
    'instance_settings' => array(),
    'default_widget' => array(
        'type' => 'hidden',
        'label' => t('SK Sales Taxes'),
        'settings' => array(),
      ),
    'default_formatter' => '',
  );

  // Field Collection for AB sales taxes
  $fields['field_sales_taxes_ca_ab'] = array(
    'field_name' => 'field_sales_taxes_ca_ab',
    'label' => t('AB Sales Taxes'),
    'description' => t('Alberta sales taxes. Check off all that apply.'),
    'type' => 'field_collection',
    'module' => 'field_collection',
    'cardinality' => 1,
    'translatable' => FALSE,
    'settings' => array(
        'hide_blank_items' => 1,
        'path' => '',
    ),
    'instance_settings' => array(),
    'default_widget' => array(
        'type' => 'hidden',
        'label' => t('AB Sales Taxes'),
        'settings' => array(),
      ),
    'default_formatter' => '',
  );

  // Field Collection for BC sales taxes
  $fields['field_sales_taxes_ca_bc'] = array(
    'field_name' => 'field_sales_taxes_ca_bc',
    'label' => t('BC Sales Taxes'),
    'description' => t('British Columbia sales taxes. Check off all that apply.'),
    'type' => 'field_collection',
    'module' => 'field_collection',
    'cardinality' => 1,
    'translatable' => FALSE,
    'settings' => array(
        'hide_blank_items' => 1,
        'path' => '',
    ),
    'instance_settings' => array(),
    'default_widget' => array(
        'type' => 'hidden',
        'label' => t('BC Sales Taxes'),
        'settings' => array(),
      ),
    'default_formatter' => '',
  );

  // Field Collection for YT sales taxes
  $fields['field_sales_taxes_ca_yt'] = array(
    'field_name' => 'field_sales_taxes_ca_yt',
    'label' => t('YT Sales Taxes'),
    'description' => t('Yukon Territory sales taxes. Check off all that apply.'),
    'type' => 'field_collection',
    'module' => 'field_collection',
    'cardinality' => 1,
    'translatable' => FALSE,
    'settings' => array(
        'hide_blank_items' => 1,
        'path' => '',
    ),
    'instance_settings' => array(),
    'default_widget' => array(
        'type' => 'hidden',
        'label' => t('YT Sales Taxes'),
        'settings' => array(),
      ),
    'default_formatter' => '',
  );

  // Field Collection for NT sales taxes
  $fields['field_sales_taxes_ca_nt'] = array(
    'field_name' => 'field_sales_taxes_ca_nt',
    'label' => t('NT Sales Taxes'),
    'description' => t('Northwest Territories sales taxes. Check off all that apply.'),
    'type' => 'field_collection',
    'module' => 'field_collection',
    'cardinality' => 1,
    'translatable' => FALSE,
    'settings' => array(
        'hide_blank_items' => 1,
        'path' => '',
    ),
    'instance_settings' => array(),
    'default_widget' => array(
        'type' => 'hidden',
        'label' => t('NT Sales Taxes'),
        'settings' => array(),
      ),
    'default_formatter' => '',
  );

  // Field Collection for NU sales taxes
  $fields['field_sales_taxes_ca_nu'] = array(
    'field_name' => 'field_sales_taxes_ca_nu',
    'label' => t('NU Sales Taxes'),
    'description' => t('Nunavut sales taxes. Check off all that apply.'),
    'type' => 'field_collection',
    'module' => 'field_collection',
    'cardinality' => 1,
    'translatable' => FALSE,
    'settings' => array(
        'hide_blank_items' => 1,
        'path' => '',
    ),
    'instance_settings' => array(),
    'default_widget' => array(
        'type' => 'hidden',
        'label' => t('NU Sales Taxes'),
        'settings' => array(),
      ),
    'default_formatter' => '',
  );

  return $fields;

} //_default_admin_area_collection_field_info()

function _default_tax_field_info() {

  $fields = array();

  $bool_field_defaults =  array(
    'field_name' => '',
    'label' => '',
    'description' => '',
    'type' => 'list_boolean',
    'module' => 'list',
    'cardinality' => 1,
    'translatable' => FALSE,
    'settings' => array(
      'allowed_values' => array(
        0 => '',
        1 => '',
      ),
      'allowed_values_function' => '',
    ),
    'instance_settings' => array(),
    'default_widget' => '',
    'default_formatter' => '',
  );

  // GST - Goods and Services Tax
  $fields['field_apply_ca_gst'] = array_merge($bool_field_defaults, array(
                                        'field_name' => 'field_apply_ca_gst',
                                        'label'      => t('PST'),
                                        ));

  // HST - Harmonized Sales Tax
  $fields['field_apply_ca_hst'] = array_merge($bool_field_defaults, array(
                                        'field_name' => 'field_apply_ca_hst',
                                        'label'      => t('PST'),
                                        ));

  // PST - Provincial Sales Tax
  $fields['field_apply_ca_pst'] = array_merge($bool_field_defaults, array(
                                        'field_name' => 'field_apply_ca_pst',
                                        'label'      => t('PST'),
                                        ));

  // PLT - Provincial Liquor Tax
  $fields['field_apply_ca_plt'] = array_merge($bool_field_defaults, array(
                                        'field_name' => 'field_apply_ca_plt',
                                        'label'      => t('PLT - Provincial Liquor Tax'),
                                        ));

  // PVT - Provincial Vehicle Tax
  $fields['field_apply_ca_pvt'] = array_merge($bool_field_defaults, array(
                                        'field_name' => 'field_apply_ca_pvt',
                                        'label'      => t('PVT - Provincial Vehicle Tax'),
                                        ));

  // NB TPPT - Tangible Personal Property Tax
  // (boat and or aircraft purchased through a private sale)
  $fields['field_apply_ca_nb_tppt'] = array_merge($bool_field_defaults, array(
                                        'field_name' => 'field_apply_ca_nb_tppt',
                                        'label'      => t('TPPT NB'),
                                        ));

  return $fields;

} // _default_tax_field_info()


/**
 *  Returns the field instance for the root field collection
 *
 */
function _default_root_collection_field_instance_info() {

  $field_instances = array();

  // Field Collection - this entity will hold all individual tax fields
  $field_instances['field_sales_taxes_ca'] = array(
    'field_name' => 'field_sales_taxes_ca',
    'label' => t('Canadian Sales Taxes'),
    'description' => '',
    'bundle' => '', // To be filled in... below
    'entity_type' => 'commerce_product',
    'required' => TRUE,
    'commerce_cart_settings' => array(
      'attribute_field' => 0,
      'attribute_widget' => 'select',
    ),
    'default_value' => NULL,
    'deleted' => '0',
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'module' => 'field_collection',
        'settings' => array(
          'add' => 'Add',
          'delete' => 'Delete',
          'description' => TRUE,
          'edit' => 'Edit',
          'view_mode' => 'full',
        ),
        'type' => 'field_collection_view',
        'weight' => 2,
      ),
      'entityreference_view_widget' => array(
        'label' => 'inline',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'line_item' => array(
        'label' => 'inline',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'node_teaser' => array(
        'label' => 'inline',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'field_collection',
      'settings' => array(),
      'type' => 'field_collection_embed',
      'weight' => '50',
    ),
  );

  return $field_instances;

} //_default_root_collection_field_instance_info()

/**
 *  Returns list of field instances for administrative area field collection
 *
 */
function _default_admin_area_collection_field_instance_info() {

  $field_instances = array();

  // Default instance settings for Administrative Area field collections
  $field_instances_defaults = array(
    'field_name' => '',
    'label' => '',
    'description' => '',
    'bundle' => 'field_sales_taxes_ca',
    'entity_type' => 'field_collection_item',
    'required' => TRUE,
    'commerce_cart_settings' => array(
      'attribute_field' => 0,
      'attribute_widget' => 'select',
    ),
    'default_value' => NULL,
    'deleted' => '0',
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'module' => 'field_collection',
        'settings' => array(
          'add' => 'Add',
          'delete' => 'Delete',
          'description' => TRUE,
          'edit' => 'Edit',
          'view_mode' => 'full',
        ),
        'type' => 'field_collection_view',
        'weight' => 2,
      ),
      'entityreference_view_widget' => array(
        'label' => 'inline',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'line_item' => array(
        'label' => 'inline',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'node_teaser' => array(
        'label' => 'inline',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'field_collection',
      'settings' => array(),
      'type' => 'field_collection_embed',
      'weight' => '0',
    ),
  );

  // Field Collection for NL sales taxes
  $field_instances['field_sales_taxes_ca_nl'] = 
    array_merge($field_instances_defaults, array(
      'field_name' => 'field_sales_taxes_ca_nl',
      'label' => t('Newfoundland and Labrador Sales Taxes'),
      'widget' => array(
        'active' => 0,
        'module' => 'field_collection',
        'settings' => array(),
        'type' => 'field_collection_embed',
        'weight' => '10',
      ),
    )
  );

  // Field Collection for NS sales taxes
  $field_instances['field_sales_taxes_ca_ns'] =
    array_merge($field_instances_defaults, array(
      'field_name' => 'field_sales_taxes_ca_ns',
      'label' => t('Nova Scotia Sales Taxes'),
      'widget' => array(
        'active' => 0,
        'module' => 'field_collection',
        'settings' => array(),
        'type' => 'field_collection_embed',
        'weight' => '15',
      ),
    )
  );

  // Field Collection for PE sales taxes
  $field_instances['field_sales_taxes_ca_pe'] = 
    array_merge($field_instances_defaults, array(
      'field_name' => 'field_sales_taxes_ca_pe',
      'label' => t('Prince Edward Island Sales Taxes'),
      'widget' => array(
        'active' => 0,
        'module' => 'field_collection',
        'settings' => array(),
        'type' => 'field_collection_embed',
        'weight' => '20',
      ),
    )
  );

  // Field Collection for NB sales taxes
  $field_instances['field_sales_taxes_ca_nb'] = 
    array_merge($field_instances_defaults, array(
      'field_name' => 'field_sales_taxes_ca_nb',
      'label' => t('New Brunswick Sales Taxes'),
      'widget' => array(
        'active' => 0,
        'module' => 'field_collection',
        'settings' => array(),
        'type' => 'field_collection_embed',
        'weight' => '25',
      ),
    )
  );

  // Field Collection for QC sales taxes
  $field_instances['field_sales_taxes_ca_qc'] = 
    array_merge($field_instances_defaults, array(
      'field_name' => 'field_sales_taxes_ca_qc',
      'label' => t('Quebec Sales Taxes'),
      'widget' => array(
        'active' => 0,
        'module' => 'field_collection',
        'settings' => array(),
        'type' => 'field_collection_embed',
        'weight' => '30',
      ),
    )
  );

  // Field Collection for ON sales taxes
  $field_instances['field_sales_taxes_ca_on'] = 
    array_merge($field_instances_defaults, array(
      'field_name' => 'field_sales_taxes_ca_on',
      'label' => t('Ontario Sales Taxes'),
      'widget' => array(
        'active' => 0,
        'module' => 'field_collection',
        'settings' => array(),
        'type' => 'field_collection_embed',
        'weight' => '35',
      ),
    )
  );

  // Field Collection for MB sales taxes
  $field_instances['field_sales_taxes_ca_mb'] = 
    array_merge($field_instances_defaults, array(
      'field_name' => 'field_sales_taxes_ca_mb',
      'label' => t('Manitoba Sales Taxes'),
      'widget' => array(
        'active' => 0,
        'module' => 'field_collection',
        'settings' => array(),
        'type' => 'field_collection_embed',
        'weight' => '40',
      ),
    )
  );

  // Field Collection for SK sales taxes
  $field_instances['field_sales_taxes_ca_sk'] = 
    array_merge($field_instances_defaults, array(
      'field_name' => 'field_sales_taxes_ca_sk',
      'label' => t('Saskatchewan Sales Taxes'),
      'widget' => array(
        'active' => 0,
        'module' => 'field_collection',
        'settings' => array(),
        'type' => 'field_collection_embed',
        'weight' => '45',
      ),
    )
  );

  // Field Collection for AB sales taxes
  $field_instances['field_sales_taxes_ca_ab'] = 
    array_merge($field_instances_defaults, array(
      'field_name' => 'field_sales_taxes_ca_ab',
      'label' => t('Alberta Sales Taxes'),
      'widget' => array(
        'active' => 0,
        'module' => 'field_collection',
        'settings' => array(),
        'type' => 'field_collection_embed',
        'weight' => '50',
      ),
    )
  );

  // Field Collection for BC sales taxes
  $field_instances['field_sales_taxes_ca_bc'] = 
    array_merge($field_instances_defaults, array(
      'field_name' => 'field_sales_taxes_ca_bc',
      'label' => t('British Columbia Sales Taxes'),
      'widget' => array(
        'active' => 0,
        'module' => 'field_collection',
        'settings' => array(),
        'type' => 'field_collection_embed',
        'weight' => '55',
      ),
    )
  );

  // Field Collection for YT sales taxes
  $field_instances['field_sales_taxes_ca_yt'] = 
    array_merge($field_instances_defaults, array(
      'field_name' => 'field_sales_taxes_ca_yt',
      'label' => t('Yukon Sales Taxes'),
      'widget' => array(
        'active' => 0,
        'module' => 'field_collection',
        'settings' => array(),
        'type' => 'field_collection_embed',
        'weight' => '60',
      ),
    )
  );

  // Field Collection for NT sales taxes
  $field_instances['field_sales_taxes_ca_nt'] = 
    array_merge($field_instances_defaults, array(
      'field_name' => 'field_sales_taxes_ca_nt',
      'label' => t('Northwest Territories Sales Taxes'),
      'widget' => array(
        'active' => 0,
        'module' => 'field_collection',
        'settings' => array(),
        'type' => 'field_collection_embed',
        'weight' => '65',
      ),
    )
  );

  // Field Collection for NU sales taxes
  $field_instances['field_sales_taxes_ca_nu'] = 
    array_merge($field_instances_defaults, array(
      'field_name' => 'field_sales_taxes_ca_nu',
      'label' => t('Nunavut Sales Taxes'),
      'widget' => array(
        'active' => 0,
        'module' => 'field_collection',
        'settings' => array(),
        'type' => 'field_collection_embed',
        'weight' => '70',
      ),
    )
  );

  return $field_instances;

}
// _default_admin_area_collection_field_instance_info()

/**
 *  Returns array of tax field instances
 *
 */
function _default_tax_field_instance_info() {

  $field_instances = array();

  $field_instances_defaults = array(
    'field_name' => '',
    'label' => '',
    'description' => '',
    'bundle' => '',
    'entity_type' => 'field_collection_item',
    'required' => FALSE,
    'default_value' => array(
      0 => array(
        'value' => 1,
      ),
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'display_label' => 1,
      ),
      'type' => 'options_onoff',
      'weight' => '0',
    ),
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => '0',
      ),
      'entityreference_view_widget' => array(
        'label' => 'inline',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => '0',
      ),
    ),
  );

  // GST
  $field_instances['field_apply_ca_gst'] =
    array_merge($field_instances_defaults, array(
      'field_name' => 'field_apply_ca_gst',
      'label'      => t('GST - Goods and Services Tax'),
      'widget' => array(
        'active' => 1,
        'module' => 'options',
        'settings' => array(
          'display_label' => 1,
        ),
        'type' => 'options_onoff',
        'weight' => '10',
      ),
    )
  );

  // HST
  $field_instances['field_apply_ca_hst'] =
    array_merge($field_instances_defaults, array(
      'field_name' => 'field_apply_ca_hst',
      'label'      => t('HST - Harmonized Sales Tax'),
      'widget' => array(
        'active' => 1,
        'module' => 'options',
        'settings' => array(
          'display_label' => 1,
        ),
        'type' => 'options_onoff',
        'weight' => '5',
      ),
    )
  );

  // PST
  $field_instances['field_apply_ca_pst'] =
    array_merge($field_instances_defaults, array(
      'field_name' => 'field_apply_ca_pst',
      'label'      => t('PST - Provincial Sales Tax'),
      'widget' => array(
        'active' => 1,
        'module' => 'options',
        'settings' => array(
          'display_label' => 1,
        ),
        'type' => 'options_onoff',
        'weight' => '15',
      ),
    )
  );

  // PLT - Provincial Liquor Tax
  $field_instances['field_apply_ca_plt'] =
    array_merge($field_instances_defaults, array(
      'field_name' => 'field_apply_ca_plt',
      'label'      => t('PLT - Provincial Liquor Tax'),
      'widget' => array(
        'active' => 1,
        'module' => 'options',
        'settings' => array(
          'display_label' => 1,
        ),
        'type' => 'options_onoff',
        'weight' => '20',
      ),
    )
  );

  // PVT - Provincial Vehicle Tax
  $field_instances['field_apply_ca_pvt'] =
    array_merge($field_instances_defaults, array(
      'field_name' => 'field_apply_ca_pvt',
      'label'      => t('PVT - Provincial Vehicle Tax'),
      'widget' => array(
        'active' => 1,
        'module' => 'options',
        'settings' => array(
          'display_label' => 1,
        ),
        'type' => 'options_onoff',
        'weight' => '25',
      ),
    )
  );

  // New Brunswick - TPPT - Tangible Personal Property Tax
  // (boat and or aircraft purchased through a private sale)
  $field_instances['field_apply_ca_nb_tppt'] =
    array_merge($field_instances_defaults, array(
      'field_name' => 'field_apply_ca_nb_tppt',
      'label'      => t('TPPT - Tangible Personal Property Tax'),
      'widget' => array(
        'active' => 1,
        'module' => 'options',
        'settings' => array(
          'display_label' => 1,
        ),
        'type' => 'options_onoff',
        'weight' => '30',
      ),
    )
  );

  return $field_instances;

} // _default_tax_field_instance_info()

/**
 * Helpers
 */

/**
 * Create all tax fields needed for the Commerce Tax Canada module
 */
function _create_fields() {
  _create_root_field_collection();
  _create_admin_area_field_collections();
  _create_field_collection_tax_fields();
}

/**
 * Create the root field collection that will hold all administrative area
 * (province/territory) tax fields
 */
function _create_root_field_collection() {

  if ( !field_info_field('field_sales_taxes_ca') ) {

    field_cache_clear();
    field_associate_fields('commerce_product');

    // Get the main field collection info
    $tax_fields = _default_root_collection_field_info();
    $collection_field = $tax_fields['field_sales_taxes_ca'];

    // Create the collection field
    $collection_field = field_create_field($collection_field);

    // Get the default tax collection field instance info
    $instances = _default_root_collection_field_instance_info();
    $instance = $instances['field_sales_taxes_ca'];

    // Get the Commerce Product bundle info
    $commerce_entities = entity_get_info('commerce_product');
    $commerce_bundles  = $commerce_entities['bundles'];

    // Create Commerce Product instances
    foreach ($commerce_bundles as $bundle_name => $commerce_bundle) {
      $instance['bundle'] = $bundle_name;
      field_create_instance($instance);
    }
  }
} // _create_root_field_collection()

/**
 * Create the collection fields that will hold the applicable tax fields for
 * each administrative area (province/territory)
 */
function _create_admin_area_field_collections() {

  field_cache_clear();
  field_associate_fields('commerce_product');

  // Get the list and info for the collection fields we want to create
  $fields = _default_admin_area_collection_field_info();
  // Get the default field instance info
  $instances = _default_admin_area_collection_field_instance_info();

  // Create the fields and instances
  foreach ($fields as $field) {
    if ( !field_info_field($field['field_name'])) { // Make sure field does not exist
      $field = field_create_field($field);
      field_create_instance($instances[$field['field_name']]);
    }
  }

} // _create_admin_area_field_collections()

/**
 * Create the collection fields that will hold the applicable tax fields for
 * each administrative area (province/territory)
 */
function _create_field_collection_tax_fields() {

  field_cache_clear();
  field_associate_fields('commerce_product');

  // Get the list of Administrative Area collection fields,
  // to which we'll assingn the various tax indicator fields
  $admin_area_collection_fields = _default_admin_area_collection_field_info();

  // Get the list and info for fields we want to create
  $tax_fields = _default_tax_field_info();

  // Get the instance info for fields we want to create
  $instances = _default_tax_field_instance_info();

  // Create the individual tax fields, then we'll make the instances
  foreach ($tax_fields as $tax_field) {
    if ( !field_info_field($tax_field['field_name'])) { // Make sure field does not exist
      $tax_field = field_create_field($tax_field);
    }
  }

  // Get the default tax field instance info
  $gst_instance = $instances['field_apply_ca_gst'];
  $hst_instance = $instances['field_apply_ca_hst'];
  $pst_instance = $instances['field_apply_ca_pst'];
  $plt_instance = $instances['field_apply_ca_plt'];
  $pvt_instance = $instances['field_apply_ca_pvt'];

  //
  // Newfoundland and Labrador - set up field default settings
  // http://www.fin.gov.nl.ca/fin/tax_programs_incentives/personal/privatesaleofvehicles.html
  // RST - Retail Sales tax applies to the private sale of vehicles, including
  // motor vehicles, snowmobiles, aircraft, boats, ships, trailers or vessels.
  //
  $collection_field_name = $admin_area_collection_fields['field_sales_taxes_ca_nl']['field_name'];
  // HST
  $hst_instance['bundle']                    = $collection_field_name;
  $hst_instance['default_value'][0]['value'] = TRUE;
  field_create_instance($hst_instance);
  // PVT
  $pvt_instance['label']                     = 'RST - Retail Sales tax (private sale of vehicles)';
  $pvt_instance['bundle']                    = $collection_field_name;
  $pvt_instance['default_value'][0]['value'] = FALSE;
  field_create_instance($pvt_instance);

  //
  // Nova Scotia - set up field default settings
  //
  $collection_field_name = $admin_area_collection_fields['field_sales_taxes_ca_ns']['field_name'];
  // HST
  $hst_instance['bundle']                    = $collection_field_name;
  $hst_instance['default_value'][0]['value'] = TRUE;
  field_create_instance($hst_instance);


  //
  // Prince Edward Island - set up field default settings
  //
  $collection_field_name = $admin_area_collection_fields['field_sales_taxes_ca_pe']['field_name'];
  // HST
  $hst_instance['bundle']                    = $collection_field_name;
  $hst_instance['default_value'][0]['value'] = TRUE;
  field_create_instance($hst_instance);

  //
  // New Brunswick - set up field default settings
  //
  // http://www.gnb.ca/0162/tax/hst-e.asp
  // PVT - Provincial Vehicle Tax (motor vehicle purchased through a private sale)
  // TPPT - Tangible Personal Property Tax (boat and or aircraft purchased through a private sale)
  //
  $collection_field_name = $admin_area_collection_fields['field_sales_taxes_ca_nb']['field_name'];
  $tppt_instance = $instances['field_apply_ca_nb_tppt'];
  // HST
  $hst_instance['bundle']                    = $collection_field_name;
  $hst_instance['default_value'][0]['value'] = TRUE;
  field_create_instance($hst_instance);
  // PVT
  $pvt_instance['label']                     = 'PVT - Provincial Vehicle Tax';
  $pvt_instance['bundle']                    = $collection_field_name;
  $pvt_instance['default_value'][0]['value'] = FALSE;
  field_create_instance($pvt_instance);
  // TPPT
  $tppt_instance['bundle']                    = $collection_field_name;
  $tppt_instance['default_value'][0]['value'] = FALSE;
  field_create_instance($tppt_instance);

  //
  // Quebec - set up field default settings
  //
  $collection_field_name = $admin_area_collection_fields['field_sales_taxes_ca_qc']['field_name'];
  // GST
  $gst_instance['bundle']                    = $collection_field_name;
  $gst_instance['default_value'][0]['value'] = TRUE;
  field_create_instance($gst_instance);
  // PST
  $pst_instance['label']                     = 'QST - Quebec Sales tax';
  $pst_instance['bundle']                    = $collection_field_name;
  $pst_instance['default_value'][0]['value'] = TRUE;
  field_create_instance($pst_instance);

  //
  // Ontario - set up field default settings
  //
  $collection_field_name = $admin_area_collection_fields['field_sales_taxes_ca_on']['field_name'];
  // GST
  $gst_instance['bundle']                    = $collection_field_name;
  $gst_instance['default_value'][0]['value'] = FALSE;
  field_create_instance($gst_instance);
  // HST
  $hst_instance['bundle']                    = $collection_field_name;
  $hst_instance['default_value'][0]['value'] = TRUE;
  field_create_instance($hst_instance);

  //
  // Manitoba - set up field default settings
  //
  $collection_field_name = $admin_area_collection_fields['field_sales_taxes_ca_mb']['field_name'];
  // GST
  $gst_instance['bundle']                    = $collection_field_name;
  $gst_instance['default_value'][0]['value'] = TRUE;
  field_create_instance($gst_instance);
  // PST
  $pst_instance['label']                     = 'RST - Retail Sales tax';
  $pst_instance['bundle']                    = $collection_field_name;
  $pst_instance['default_value'][0]['value'] = TRUE;
  field_create_instance($pst_instance);
  // PVT
  $pvt_instance['label']                     = 'PVT - Provincial Vehicle Tax';
  $pvt_instance['bundle']                    = $collection_field_name;
  $pvt_instance['default_value'][0]['value'] = FALSE;
  field_create_instance($pvt_instance);

  //
  // Saskatchewan - set up field default settings
  //
  $collection_field_name = $admin_area_collection_fields['field_sales_taxes_ca_sk']['field_name'];
  // GST
  $gst_instance['bundle']                    = $collection_field_name;
  $gst_instance['default_value'][0]['value'] = TRUE;
  field_create_instance($gst_instance);
  // PST
  $pst_instance['label']                     = 'PST - Provincial Sales tax';
  $pst_instance['bundle']                    = $collection_field_name;
  $pst_instance['default_value'][0]['value'] = TRUE;
  field_create_instance($pst_instance);
  // PLT
  $plt_instance['label']                     = 'LCT - Liquor Consumption Tax';
  $plt_instance['bundle']                    = $collection_field_name;
  $plt_instance['default_value'][0]['value'] = FALSE;
  field_create_instance($plt_instance);

  //
  // Alberta - set up field default settings
  //
  $collection_field_name = $admin_area_collection_fields['field_sales_taxes_ca_ab']['field_name'];
  // GST
  $gst_instance['bundle']                    = $collection_field_name;
  $gst_instance['default_value'][0]['value'] = TRUE;
  field_create_instance($gst_instance);

  //
  // British Columbia - set up field default settings
  //
  $collection_field_name = $admin_area_collection_fields['field_sales_taxes_ca_bc']['field_name'];
  // GST
  $gst_instance['bundle']                    = $collection_field_name;
  $gst_instance['default_value'][0]['value'] = TRUE;
  field_create_instance($gst_instance);
  // PST
  $pst_instance['label']                     = 'PST - Provincial Sales tax';
  $pst_instance['bundle']                    = $collection_field_name;
  $pst_instance['default_value'][0]['value'] = TRUE;
  field_create_instance($pst_instance);

  //
  // Yukon - set up field default settings
  //
  $collection_field_name = $admin_area_collection_fields['field_sales_taxes_ca_yt']['field_name'];
  // GST
  $gst_instance['bundle']                    = $collection_field_name;
  $gst_instance['default_value'][0]['value'] = TRUE;
  field_create_instance($gst_instance);

  //
  // Northwest Territories - set up field default settings
  //
  $collection_field_name = $admin_area_collection_fields['field_sales_taxes_ca_nt']['field_name'];
  // GST
  $gst_instance['bundle']                    = $collection_field_name;
  $gst_instance['default_value'][0]['value'] = TRUE;
  field_create_instance($gst_instance);

  //
  // Nunavut - set up field default settings
  //
  $collection_field_name = $admin_area_collection_fields['field_sales_taxes_ca_nu']['field_name'];
  // GST
  $gst_instance['bundle']                    = $collection_field_name;
  $gst_instance['default_value'][0]['value'] = TRUE;
  field_create_instance($gst_instance);

} // _create_field_collection_tax_fields()

/**
 *
 *  Delete fields created for the Commerce Tax Canada module.
 *
 *  Note that the individual tax fields are grouped under a Field Collection.
 *  When a collection is deleted, all its tax fields are also deleted
 *  (probably by the Field Collection module).
 *
 *  At the moment, there is only one collection.
 *
 */
function _delete_fields() {

  field_cache_clear();
  field_associate_fields('commerce_tax_canada');

  // Get the Commerce Product bundle info
  $commerce_entities = entity_get_info('commerce_product');
  $commerce_bundles  = $commerce_entities['bundles'];

  // Initialize the field collection instance structure
  $instance = array(
    'field_name' => 'field_sales_taxes_ca',
    'bundle' => '',
    'entity_type' => 'commerce_product',
  );

  if (field_info_field('field_sales_taxes_ca')) {
    foreach ($commerce_bundles as $bundle_name => $bundle) {
      $instance['bundle']     = $bundle_name;
      field_delete_instance($instance);
    } // foreach
  } // if

  // Now really delete them... 5 at a time
//  field_purge_batch(5);

  return;

} // _delete_fields()
